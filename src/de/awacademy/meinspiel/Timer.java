package de.awacademy.meinspiel;

import de.awacademy.meinspiel.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private Model model;
    private Graphics graphics;

    long lastMillis = -1;

    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long now) {
        long millis = now / 1000000;

        long deltaMillis = 0;

        if (lastMillis != -1) {
            deltaMillis = millis - lastMillis;
        }
        this.model.update(deltaMillis);
        lastMillis = millis;

        // das zum Ändern
        if (model.getCounter() <= 20000) {//5000 = 5sec
            graphics.drawRegeln();
        }
        if (model.getCounter() > 20000) {
            graphics.draw();
        }
    }
}
