package de.awacademy.meinspiel.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    private int counter;
    private long counterMüll = 0;
    private Müllstück gewähltesMüllstück = new Müllstück();
    private int gewinnerSchwelleLevel1 = 6;// oben und unten nochmal bei reset
    private int verliererSchwelleLevel1 = 8;
    private int geschwindigkeitNeueRandomMüllstke = 2000;// in Millisek -> 2000 - 2sec
    private int geschwindigkeitMoveLevel1 = 3;
    private int hoeheStapel = 15;// Wie stark sich der Stapel unsortierten Mülls erhöhen soll


    private List<Müllstück> müllstücks = new LinkedList<>();
    //private List<GestapeltesMüllstück>gestapeltesMüllstücks = new LinkedList<>();
    //nur gelbes, statisches, was dann im Coontainer liegt
    private List<Müllstück> gesammeltesPlastiks = new LinkedList<>();// im folgenden nicht nur Plastik, auch anderes Gesammeltes
    //für nicht-gesammeltes eine Liste
    private List<Müllstück> gestapeltesMüllstücks = new LinkedList<>();

    // getter Gesammeltes
    public List<Müllstück> getGesammeltesPlastiks() {
        return gesammeltesPlastiks;
    }

    //getter Stapel - nicht gesammelt
    public List<Müllstück> getGestapeltesMüllstücks() {
        return gestapeltesMüllstücks;
    }

    Random rand = new Random();

    // erstelle Müll, inkl. GewinnerStop
    public void erstelleMüll(long deltaMillis) {
        if (counter > 20000) {//hier vorher 5000 Start nach 5Sec - auch unten ändern
            this.counterMüll += deltaMillis;
            if (getGesammeltesPlastiks().size() < gewinnerSchwelleLevel1 && getGestapeltesMüllstücks().size() < verliererSchwelleLevel1) {// hier kleiner Gewinnerschwelle, dann bei = Schwelle Stop
                if (this.counterMüll >= geschwindigkeitNeueRandomMüllstke) {//2sec - 2000millisec // Geschwindigkeit setzen: generiere neue Müllstke
                    // random Farbe, ranadom X wert, Start bei Y=0
                    müllstücks.add(new Müllstück(rand.nextInt(4) + 1, rand.nextInt(899) + 1, 0));
                    counterMüll = 0;// this.
                }
            }
        }
    }


    //Methode, damit Müll von oben fallen soll
    public void move() {
        if (getGesammeltesPlastiks().size() < gewinnerSchwelleLevel1 && getGestapeltesMüllstücks().size() < verliererSchwelleLevel1) {
            for (Müllstück müllstück : müllstücks) {
                double neuePosition = müllstück.getPosY();
                neuePosition += geschwindigkeitMoveLevel1;// wie schnell runter : move // initial 1
                müllstück.setPosY(neuePosition);
            }
        }
    }

    // stop Bewegung, wenn nicht rechtzeitig zugeordnet -> Zuordnung in Stapel Liste
    public void stop() {
        if (!müllstücks.isEmpty()) {
            for (Müllstück müllstück : müllstücks) {
                double finalePosition = müllstück.getPosY();
                if ((finalePosition >= 450)) {
                    müllstück.imFall = false;
                    müllstück.gestoppt = true;//getMüllstücks().remove(müllstück);// wenn es hier steht, dann Fehler, weil Iteration und changes in 2 Listen
                    gestapeltesMüllstücks.add(müllstück);
                    //Position setzen
                    for (int i = 0; i <= 15; i++) {
                        müllstück.setPosX(980);
                        müllstück.setPosY(495 - hoeheStapel * gestapeltesMüllstücks.size() - 1);//hoehe, wie stark anwachsen//-5*i zeigt kein rot mehr an
                    }
                }
            }
            müllstücks.removeAll(gestapeltesMüllstücks);
        }
    }


    //Müllstück
    //getter setter:
    //gewähltes Stk:
    public Müllstück getGewähltesMüllstück() {
        return gewähltesMüllstück;
    }

    public void setGewähltesMüllstück(Müllstück gewähltesMüllstück) {
        this.gewähltesMüllstück = gewähltesMüllstück;
    }

    // getter Müllstücks -Liste
    public List<Müllstück> getMüllstücks() {
        return müllstücks;
    }

    //Methode Müllstück finden
    public Müllstück findeMüllstück(double x, double y) {
        for (Müllstück müllstück : müllstücks) {
            if (müllstück.imFall) {
                if (Math.abs(müllstück.getPosX() - x) <= 100 && Math.abs(müllstück.getPosY() - y) <= 80) {//80
                    return müllstück;
                }
            }
        }
        return null;
    }

    //Spiel stoppen -final verloren & stoppen Gewinn
    //verlieren
    public int getVerliererSchwelleLevel1() {
        return verliererSchwelleLevel1;
    }

    //gewinnen
    public int getGewinnerSchwelleLevel1() {
        return gewinnerSchwelleLevel1;
    }

    public void setGewinnerSchwelleLevel1(int gewinnerSchwelleLevel1) {
        this.gewinnerSchwelleLevel1 = gewinnerSchwelleLevel1;
    }

    //Update:
    public void update(long deltaMills) {
        counter += deltaMills;

        erstelleMüll(deltaMills);
        move();
        stop();
    }

    //Methode zum Reset, stelle alles wieder auf Null
    public void resetGame() {
        müllstücks.clear();
        gesammeltesPlastiks.clear();
        gestapeltesMüllstücks.clear();

        resetCounter();
        counterMüll = 0;

        //alle Attribute nochmal neu
        counter = 60000;// siehe oben
        counterMüll = 0;
        gewähltesMüllstück = new Müllstück();
        gewinnerSchwelleLevel1 = 6;// auch oben ändern
        verliererSchwelleLevel1 = 8;
        geschwindigkeitNeueRandomMüllstke = 2000;// in Millisek -> 2000 - 2sec
        geschwindigkeitMoveLevel1 = 1;
        hoeheStapel = 15;// Wie stark sich der Stapel unsortierten Mülls erhöhen soll
        Random rand = new Random();
    }


    public void resetCounter() {
        counter = 0;
    }

    public int getCounter() {
        return counter;
    }
}
