package de.awacademy.meinspiel.model;

import java.util.List;

public class Müllstück {
    private double posX;
    private double posY;
    public int müllzahl;//Zahl Plastik =1; Zahl Papier = 2; Zahl Elektro = 3; Zahl Glas = 4;
    public boolean imFall;
    public boolean gestoppt;

    public Müllstück(int müllzahl, double posX, double posY) {
        this.imFall = true;
        this.gestoppt = false;
        this.müllzahl = müllzahl;
        this.posX = posX;
        this.posY = posY;
    }

    public Müllstück() {
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    //X Werte ändern, wenn ausgewählt und: dem richtigen Container zugeordnet
    public double changeX(char c) {
        if (müllzahl == 1 && c == 'A') { // 1 = gelbes Müllstk = Plastik
            this.posX = 45;
        }
        if (müllzahl == 2 && c == 'S') { // 2 = blaues Müllstk = Papier
            this.posX = 275;
        }
        if (müllzahl == 3 && c == 'D') { // 3 = graues Müllstk = Elektromüll
            this.posX = 495;
        }
        if (müllzahl == 4 && c == 'F') { // 4= grünes Müllstk = Glas
            this.posX = 725;
        }
        return posX;
    }

    //Y Werte ändern
    public double changeY(char c) {
        Model model = new Model();

        if (müllzahl == 1 && c == 'A') { // 1 = gelbes Müllstk = Plastik
            this.posY = 430 - 150 * model.getGesammeltesPlastiks().size();
        }
        if (müllzahl == 2 && c == 'S') { // 2 = blaues Müllstk = Papier
            this.posY = 430;
        }
        if (müllzahl == 3 && c == 'D') { // 3 = graues Müllstk = Elektromüll
            this.posY = 430;
        }
        if (müllzahl == 4 && c == 'F') { // 4= grünes Müllstk = Glas
            this.posY = 430;
        }
        return posY;
    }
}
