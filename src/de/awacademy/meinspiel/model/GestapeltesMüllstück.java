package de.awacademy.meinspiel.model;

public class GestapeltesMüllstück {
    private double posX;
    private double posY;
    public int müllzahl;

    public GestapeltesMüllstück() {
        this.posX = 1000;//posX
        this.posY = 60;//posY
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }
}
