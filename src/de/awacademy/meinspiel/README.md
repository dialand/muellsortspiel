Dieses Projekt war mein erstes Projekt im Rahmen des Accelerated Learning Kurses
in Java. Ich habe ein Müllsort Spiel entwickelt, weil ich beim Umzug nach München
erstaut war, das es hier keine Plastikmülltrennung bzgl. Plastikmüll gibt.

In diesem Spiel trennt man Müll und es ist parallel dazu noch für die linke Hand
 eine gute Übung zum 10-Finger-Schreibsystem.