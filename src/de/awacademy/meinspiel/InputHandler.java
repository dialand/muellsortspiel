package de.awacademy.meinspiel;

import de.awacademy.meinspiel.model.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }


    // Auswahl bei Klick- dieses gewählte Müllstk
    public void onClick(MouseEvent event) {
        Müllstück müllstück = model.findeMüllstück(event.getX(), event.getY());
        if (müllstück != null) {
            model.setGewähltesMüllstück(müllstück);
        }
    }

    // Zuordnung -Müll nur jeweils in den richtigen Container, dann in die Gesammelt-Liste
    public void onKeyReleased(KeyEvent event) {

        if (model.getGewähltesMüllstück().gestoppt != true) {
            //if müllzahl =1, dann Plastik zu gelben Container
            if (event.getCode() == KeyCode.A && model.getGewähltesMüllstück().müllzahl == 1) {
                model.getMüllstücks().remove(model.getGewähltesMüllstück());
                model.getGesammeltesPlastiks().add(model.getGewähltesMüllstück());
                model.getGewähltesMüllstück().changeX('A');
                model.getGewähltesMüllstück().changeY('A');
            }
            if (event.getCode() == KeyCode.S && model.getGewähltesMüllstück().müllzahl == 2) {
                model.getMüllstücks().remove(model.getGewähltesMüllstück());
                model.getGesammeltesPlastiks().add(model.getGewähltesMüllstück());
                model.getGewähltesMüllstück().changeX('S');
                model.getGewähltesMüllstück().changeY('S');
            }
            if (event.getCode() == KeyCode.D && model.getGewähltesMüllstück().müllzahl == 3) {
                model.getMüllstücks().remove(model.getGewähltesMüllstück());
                model.getGesammeltesPlastiks().add(model.getGewähltesMüllstück());
                model.getGewähltesMüllstück().changeX('D');
                model.getGewähltesMüllstück().changeY('D');
            }
            if (event.getCode() == KeyCode.F && model.getGewähltesMüllstück().müllzahl == 4) {
                model.getMüllstücks().remove(model.getGewähltesMüllstück());
                model.getGesammeltesPlastiks().add(model.getGewähltesMüllstück());
                model.getGewähltesMüllstück().changeX('F');
                model.getGewähltesMüllstück().changeY('F');
            }
        }

        if (model.getGesammeltesPlastiks().size() >= model.getGewinnerSchwelleLevel1() ||
                model.getGestapeltesMüllstücks().size() >= model.getVerliererSchwelleLevel1()) {
            if (event.getCode() == KeyCode.R) {
                model.resetGame();
            }
        }


    }
}