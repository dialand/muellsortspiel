package de.awacademy.meinspiel;

import de.awacademy.meinspiel.model.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;


public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }


    public void drawRegeln() {
        //Hintergrund
        gc.setFill(Color.ALICEBLUE);
        gc.fillRect(0, 0, 1200, 550);
        gc.setFont(new Font("Arial", 32));
        gc.setFill(Color.BLACK);
        gc.fillText("Müll-Sort", 550, 80, 200);
        //Legende
        gc.setFont(new Font("Arial", 26));
        gc.setFill(Color.BLACK);
        gc.fillText("Es gibt so viel Müll:\n", 350, 180, 400);
        gc.fillText("Plastik: ", 240, 230, 200);
        gc.fillText("Papier: ", 240, 280, 200);
        gc.fillText("Elektro: ", 240, 330, 200);
        gc.fillText("Glas: ", 240, 380, 200);
        gc.setFill(Color.YELLOW);
        gc.fillRect(400, 207, 50, 25);
        gc.setFill(Color.BLUE);
        gc.fillRect(400, 257, 50, 25);
        gc.setFill(Color.GREY);
        gc.fillRect(400, 307, 50, 25);
        gc.setFill(Color.GREEN);
        gc.fillRect(400, 357, 50, 25);
        // Erklärung Tasten:
        gc.setFont(new Font("Arial", 26));
        gc.setFill(Color.BLACK);
        gc.fillText("Bitte sortiere es! Press key: ", 750, 180, 400);
        gc.fillText("A", 750, 230);
        gc.fillText("S", 750, 280);
        gc.fillText("D", 750, 330);
        gc.fillText("F", 750, 380);
    }

    public void draw() {

        //Hintergrund

        gc.setFill(Color.ALICEBLUE);
        gc.fillRect(0, 0, 1200, 550);

        //Hintergrund
        //Boden
        gc.setFill(Color.rgb(210, 180, 140));
        gc.fillRect(0, 520, 1200, 100);

        //Plastik Container
        gc.setFill(Color.YELLOW);
        gc.fillRect(20, 450, 150, 85);
        gc.setFont(new Font("Arial", 14));
        gc.setFill(Color.BLACK);
        gc.fillText("Plastik", 90, 480, 500);
        gc.fillText("Press: A", 90, 510, 500);
        gc.setTextAlign(TextAlignment.CENTER);

        //Papier Container
        gc.setFill(Color.BLUE);
        gc.fillRect(250, 450, 150, 85);
        gc.setFont(new Font("Arial", 14));
        gc.setFill(Color.BLACK);
        gc.fillText("Papier", 320, 480, 500);
        gc.fillText("Press: S", 320, 510, 500);
        gc.setTextAlign(TextAlignment.CENTER);

        //Elektro Container
        gc.setFill(Color.GRAY);
        gc.fillRect(470, 450, 150, 85);
        gc.setFont(new Font("Arial", 14));
        gc.setFill(Color.BLACK);
        gc.fillText("Elektromüll", 540, 480, 500);
        gc.fillText("Press: D", 540, 510, 500);
        gc.setTextAlign(TextAlignment.CENTER);

        //Glas Container
        gc.setFill(Color.GREEN);
        gc.fillRect(700, 450, 150, 85);
        gc.setFont(new Font("Arial", 14));
        gc.setFill(Color.BLACK);
        gc.fillText("Glas", 770, 480, 500);
        gc.fillText("Press: F", 770, 510, 500);
        gc.setTextAlign(TextAlignment.CENTER);

        //Müllstück Farben:
        //INSERT: FARBEN Der 4 ARTEN:
        for (Müllstück müllstück : model.getMüllstücks()) {

            //Markierung
            if (müllstück == model.getGewähltesMüllstück()) {
                if (müllstück.müllzahl == 1) {
                    gc.setFill(Color.rgb(255, 185, 15));
                } else if (müllstück.müllzahl == 2) {
                    gc.setFill(Color.rgb(99, 184, 255));
                } else if (müllstück.müllzahl == 3) {
                    gc.setFill(Color.rgb(181, 181, 181));
                } else if (müllstück.müllzahl == 4) {
                    gc.setFill(Color.rgb(152, 251, 152));
                } else {
                    throw new IllegalStateException();
                }
            }

            //Start Farben
            else {
                if (müllstück.müllzahl == 1) {
                    gc.setFill(Color.YELLOW);
                } else if (müllstück.müllzahl == 2) {
                    gc.setFill(Color.BLUE);
                } else if (müllstück.müllzahl == 3) {
                    gc.setFill(Color.GRAY);
                } else if (müllstück.müllzahl == 4) {
                    gc.setFill(Color.GREEN);
                } else {
                    throw new IllegalStateException();
                }
            }
            gc.fillRect(müllstück.getPosX() - 50, müllstück.getPosY() - 20, 100, 40);
        }

        //Gesammelte Stke - richtige Farbe
        for (Müllstück gesammeltesStk : model.getGesammeltesPlastiks()) {
            if (gesammeltesStk.müllzahl == 1) {
                gc.setFill(Color.YELLOW);
            } else if (gesammeltesStk.müllzahl == 2) {
                gc.setFill(Color.BLUE);
            } else if (gesammeltesStk.müllzahl == 3) {
                gc.setFill(Color.GRAY);
            } else if (gesammeltesStk.müllzahl == 4) {
                gc.setFill(Color.GREEN);
            }
            gc.fillRect(gesammeltesStk.getPosX(), gesammeltesStk.getPosY(), 100, 40);
        }
        //für nicht gesammelte Stk, Stapel
        //für Stapel:
        for (Müllstück gestapeltesMüllstück : model.getGestapeltesMüllstücks()) {
            gc.setFill(Color.RED);
            //gc.setFill(Color.rgb(72,118,255));
            gc.fillRect(gestapeltesMüllstück.getPosX(), gestapeltesMüllstück.getPosY(), 100, 40);
            gc.strokeRect(gestapeltesMüllstück.getPosX(), gestapeltesMüllstück.getPosY(), 100, 40);
        }

        //für gesamtZahl - sortiert, gesammelte Müllstücke
        gc.setFont(new Font("Arial", 18));
        gc.setFill(Color.BLACK);
        gc.fillText("gesammelte\nMüllstücke:\n", 1030, 80, 500);
        gc.setTextAlign(TextAlignment.CENTER);
        if (model.getGesammeltesPlastiks().size() >= 0) {
            gc.fillText("\n\n" + String.valueOf(model.getGesammeltesPlastiks().size()), 1030, 95);
        }


        //für Gewinneranzeige
        if (model.getGesammeltesPlastiks().size() >= model.getGewinnerSchwelleLevel1()) {
            gc.setFont(new Font("Arial", 40));
            gc.setFill(Color.BLACK);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("Glückwunsch!\nDu bist Müllsortier-Meister!", 550, 80, 1200);
            gc.fillText("Press \"R\" to restart", 550, 250, 1200);
        }

        // für GameOver Nachricht
        if (model.getGestapeltesMüllstücks().size() >= model.getVerliererSchwelleLevel1()) {//15
            gc.setFont(new Font("Arial", 40));
            gc.setFill(Color.RED);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText("Übe weiter!\nNoch gibt es zu viel unsortierten Müll.", 550, 80, 1200);
            gc.fillText("Press \"R\" to restart", 550, 250, 1200);
        }

    }
}
